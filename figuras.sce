// Parametros

titulo = 'Salidas del sistema vs tiempo';
//titulo = 'Señal de control vs tiempo';
//titulo = 'Señales de error vs tiempo';

texto_eje_x = 't(s)';
texto_eje_y = 'y';
//texto_eje_y = 'F(N)';
//texto_eje_y = 'e';

tamaño_titulo = 5;
tamaño_eje_x = 4;
tamaño_eje_y = 4;

leyenda = ['x (m)','theta (rad)']
//leyenda = ['error en x (m)','error en theta (rad)']

posicion = 4
// 1: upper right
// 2: upper left
// 3: lower left
// 4: lower right
// more: https://help.scilab.org/docs/6.0.2/en_US/legend.html

// Modificar imagen

fig = gcf();
a = gca();

a.title.text = titulo;
a.title.font_size = tamaño_titulo;

a.x_label.text = texto_eje_x;
a.x_label.font_size = tamaño_eje_x;

a.y_label.text = texto_eje_y;
a.y_label.font_size = tamaño_eje_y;

h1 = legend(leyenda, pos = posicion);
