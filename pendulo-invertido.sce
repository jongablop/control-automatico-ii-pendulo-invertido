//Programa para diseñar controlador LQG de un péndulo invertido
// Autor: Jon Gabirondo López (jgabirondo001@ikasle.ehu.eus)

s = %s;

// Valores de las constantes
M = 0.5;
m = 0.2;
L = 0.5;
b = 0.1;
g = 9.8;

// Definición del sistema

// Se define el divisor común de las matrices A y B
d = (7.0*M + 4.0*m)*L

A = 1.0/d * [
0, d, 0, 0;
0, -7.0*b*L, 3.0*m*g*L, 0;
0, 0, 0, d;
0, -6.0*b, 6.0*(M + m)*g/d, 0;
]

B = 1.0/d * [
0;
7.0*L;
0;
6.0;
]

C = [
1, 0;
0, 0;
0, 1;
0, 0;
]

D = [0;0;0;0;]

I = eye(A);

pol_car = det(s*I - A)

disp(pol_car,'Polinomio característico:')

raices = roots(pol_car)

sistema = syslin('c', A, B, C');

// Comprobar controlabilidad

P_c = [B, A*B, A*A*B, A*A*A*B]
disp(rank(P_c), 'Rango matriz controlabilidad:')

// Calcular controlador LQR

// Comentar las definiciones de Q que no se utilicen
Q = eye(A);
Q = C*C';

Q = [
3000 0 0 0;
0 0 0 0;
0 0 6000 0;
0 0 0 0;
]

R = 1

[K, P] = lqr(sistema, Q, R)
disp(spec(A + B*K), 'Polos controlador')

K_r = -1.0/(C'*inv(A+B*K)*B)
disp(K_r, 'K_r:')


// Comprobar observabilidad

P_o = [C'; C'*A; C'*A*A; C'*A*A*A]
disp(rank(P_o), 'Rango matriz observabilidad:')

// Comentar las definiciones de W que no se utilicen
W = eye(A)
V = [1 0; 0 1]

W = [
1000 0 0 0;
0 0 0 0;
0 0 750 0;
0 0 0 0;
]

V = [100 0;0 1000]

[Lo, Po] = lqe(sistema, W, V)
disp(spec(A + Lo*C'), 'Polos observador')
