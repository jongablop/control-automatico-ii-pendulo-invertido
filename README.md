# Controlador LQG para un péndulo invertido

En este repositorio se recogen los programas de Scilab y Xcos utilizados para diseñar un controlador lineal cuadrático gaussiano para un péndulo invertido. Este controlador forma parte de un trabajo para la asignatura de Control Automático II de la UPV/EHU.

En dicho trabajo se ha diseñado un controlador Lineal Cuadrático Gaussiano con la intención de controlar un péndulo invertido. Para ello ha sido necesario realizar un modelo matemático teniendo en cuenta varias aproximaciones que permiten linealizar la respuesta del sistema. Después de observar sus características en lazo abierto, se ha cerrado el lazo mediante un controlador óptimo para finalmente implementar un observador óptimo. En todo el proceso del diseño se han utilizado Scilab y Xcos para realizar simulaciones númericas de las respuestas del sistema.
